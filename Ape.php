<?php

require_once('Animal.php');

class Ape extends Animal{

    public $name;
    public $legs = 2;
    public $cold_blooded = "no";
    public function __construct($ape_name)
    {   
        $this->name=$ape_name;
    }
    function yell(){
        echo "Auooo";
    }
}