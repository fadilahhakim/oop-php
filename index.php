<?php
    require_once('Animal.php');
    require_once('Ape.php');
    require_once('Frog.php');

    
$sheep = new Animal("shaun");

echo "Name :" . $sheep->name . "<br>"; // "shaun"
echo "Legs :" . $sheep->legs . "<br>"; // 4
echo "cold_blooded :". $sheep->cold_blooded . "<br><br>"; // "no"


// index.php
$sungokong = new Ape("kera sakti");
echo "Name :" . $sungokong->name . "<br>";
echo "Legs :" . $sungokong->legs . "<br>";
echo "cold_blooded :" . $sungokong->cold_blooded . "<br>" . "Yell :"; 
echo $sungokong->yell() . "<br><br>";// "Auooo"

$kodok = new Frog("buduk");
echo "Name :" . $kodok->name . "<br>";
echo "Legs :" . $kodok->legs . "<br>";
echo "cold_blooded :" . $kodok->cold_blooded . "<br>" . "Jump :"; 
$kodok->jump() ; // "hop hop"


?>